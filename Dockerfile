FROM ubuntu:latest

WORKDIR /usr/bin
RUN apt-get update && apt-get install -yqq curl && apt-get clean
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
COPY isr-deploy /usr/bin/isr-deploy
RUN chmod ugo+x isr-deploy
RUN chmod ugo+x kubectl
WORKDIR /tmp
