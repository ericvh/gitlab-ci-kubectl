#!/bin/bash -e

[[ "$TRACE" ]] && set -x

function check_required_vars() {
    if [[ -z "$CI_KUBE_TOKEN" ]]; then
      echo "In order to deploy, CI_KUBE_TOKEN variables must be set"
      false
    fi
}
function set_ci_context() {
    if [[ -z "$CI_COMMIT_TAG" ]]; then
      export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
      export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
    else
      export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
      export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
    fi
}
function kube_set_cred() {
    kubectl config set-credentials myself --token=$CI_KUBE_TOKEN
    kubectl config set-cluster ci --server=$CI_KUBE_SERVER
    kubectl config set-context CI --user=myself --cluster=ci
    kubectl config use-context CI
    create_secret
}
function create_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi
    kubectl create secret \
      docker-registry gitlab-registry \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run | kubectl replace --force -f -
}
function build_deploy_yaml() {
    if [[ -f $CI_KUBE_DEPLOY_YAML ]]; then
       envsubst < $CI_KUBE_DEPLOY_YAML > gitlab-ci-kube.yml
    else
      cat <<EOF > gitlab-ci-kube.yml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: $CI_KUBE_NAME
spec:
  selector:
    matchLabels:
      name: $CI_KUBE_NAME
  template:
    metadata:
      labels:
        name: $CI_KUBE_NAME
    spec:
      nodeSelector:
        use: $CI_KUBE_ENV            
      dnsPolicy: ClusterFirstWithHostNet
      imagePullSecrets:
      - name: gitlab-registry
      containers:
      - name: $CI_KUBE_NAME
        image: $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 200m
            memory: 10Mi
          requests:
            cpu: 200m
            memory: 10Mi
EOF
    fi
    [[ "$TRACE" ]] && cat -n gitlab-ci-kube.yml
    echo gitlab-ci-kube processed
}

##
## End Helper functions

option=$1
case $option
in
  build_deploy_yaml) build_deploy_yaml ;;
  create_secret) create_secret ;;
  kube_set_cred) kube_set_cred ;;
  set_ci_context) set_ci_context ;;
  check_required_vars) check_required_vars ;;
  *) exit 1 ;;
esac


